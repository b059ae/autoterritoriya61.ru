<?php
/**
 * View page
 * @var Page $model
 */
// Set meta tags
$this->pageTitle = ($model->meta_title) ? $model->meta_title : $model->title;
$this->pageKeywords = $model->meta_keywords;
$this->pageDescription = $model->meta_description;
?>

<h1 class="has_background"><?php echo $model->title; ?></h1>
<p>
    <span class="big_text">Автотерритория</span>
</p>
<p>
    г. Ростов-на-Дону, ул. <span class="big_text">Мадояна 198 Б</span>
</p>
<p>
    <span class="icon phone"></span>
    <a class="tel" href="tel:+88632189038">+7 (863)<span class="big_text">218-90-38</span></a>
    <a class="tel" href="tel:+89885513635">+7 (988)<span class="big_text">551-36-35</span></a>
</p>
<p>
    <script type="text/javascript" charset="utf-8" src="//api-maps.yandex.ru/services/constructor/1.0/js/?sid=v8_7_jHygS3U6EP37cJwIuL8LvJFPuTk&width=600&height=450"></script>
</p>