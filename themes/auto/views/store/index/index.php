<?php
$this->pageDescription='Продажа и установка автосигнализаций StarLine, парктроников, ксенона';
$this->pageKeywords='автосигнализации, сигнализация, установка сигнализации, ксенон, биксенон, парктроники, иммобилайзер, старлайн, starline, starline сигнализация, starline ростов, автосигнализации ростов';

/**
 * Site start view
 */
?>

<div class="wide_line">
	<span>Партнеры компании</span>
</div>
<div id="partners" class="row">
	<div class="col-sm-4 partner">
		<div class="partner-img">
			<a href="http://ultrastar.ru" target="_blank" rel="nofollow">
				<img
					src="/uploads/importImages/partners/starline.png"/>
			</a>
		</div>
		<h5>Авторизованный установочный центр StarLine</h5>
	</div>
	<div class="col-sm-4 partner">
		<div class="partner-img">
			<a href="http://www.kondrashov-lab.ru/" target="_blank" rel="nofollow">
				<img
					src="/uploads/importImages/partners/kondrashov.png"/>
			</a>
		</div>
		<h5>Лаборатория Андрея Кондрашова</h5>
	</div>
	<div class="col-sm-4 partner">
		<div class="partner-img">
			<a href="http://autolis.ru/" target="_blank" rel="nofollow">
				<img alt="АВТОЛИС"
					 src="/uploads/importImages/partners/autolis.jpg"/>
			</a>
		</div>
		<h5>АВТОЛИС</h5>
	</div>
</div>

<div class="wide_line">
	<span>Популярные товары</span>
</div>

<div class="products_list">
	<?php
		foreach($popular as $p)
			$this->renderPartial('_product', array('data'=>$p));
	?>
</div>

<?php $this->beginClip('underFooter'); ?>
<div style="clear:both;"></div>

<script type="text/javascript">
	$(document).ready(function(){
		$("#shares .share_list ul li a").click(function(){
			$("#shares .share_list ul li").removeClass('active');
			$(this).parent().addClass('active');
			$("#shares .products_list").load('/store/index/renderProductsBlock/'+$(this).attr('href'));
			return false;
		});
	});
</script>

<div id="shares">
	<div class="shared_products">
		<div class="share_list">
			<ul>
				<li class="active"><a href="newest">Новинки</a></li>
				<li><a href="added_to_cart">Хиты продаж</a></li>
			</ul>
		</div>

		<div style="clear:both;"></div>

		<div class="products_list">
			<?php
			foreach($newest as $p)
				$this->renderPartial('_product', array('data'=>$p));
			?>
		</div>
	</div>
</div>

<?php
// Fancybox ext
$this->widget('application.extensions.fancybox.EFancyBox', array(
	'target'=>'a.cert',
));
?>
<div class="centered">
    <div class="wide_line">
		<span>Сертификаты</span>

	</div>
        <a href="/uploads/importImages/f3.jpg" class="cert"><img alt="" src="/uploads/importImages/thumbs/f3.jpg" class="img-thumbnail"></a>
        <a href="/uploads/importImages/f1.jpg" class="cert"><img alt="" src="/uploads/importImages/thumbs/f1.jpg" class="img-thumbnail"></a>
        <a href="/uploads/importImages/f2.jpg" class="cert"><img alt="" src="/uploads/importImages/thumbs/f2.jpg" class="img-thumbnail"></a>
	<!--<div class="wide_line">
		<span>Новости</span>

	</div>

	<ul class="news">
		<?php foreach($news as $n): ?>
		<li>
			<span class="date"><?php echo $n->created ?></span>
			<a href="<?php echo $n->viewUrl ?>" class="title"><?php echo $n->title ?></a>
			<p><?php echo $n->short_description ?></p>
		</li>
		<?php endforeach; ?>
	</ul>

	<div class="all_news">
		<a href="<?=$n->category->viewUrl?>">Все новости</a>
	</div>-->
</div>
<?php $this->endClip(); ?>