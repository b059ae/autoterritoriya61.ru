<html>
<body>

<p>Здравствуйте.</p>
<p>Новый заказ номер <?= $order->id ?> принят.</p>

<ul>
    <?php foreach ($order->products as $product)
        echo '<li>' . $product->getRenderFullName() . "</li>";
    ?>
</ul>

<p>
    <b>Сумма заказа:</b>
    <?= StoreProduct::formatPrice($order->total_price + $order->delivery_price) ?> <?= Yii::app()->currency->main->symbol ?>
</p>

<p>
    <b>Контактные данные:</b><br/>
    <?= implode('<br/>', array($order->user_name, $order->user_phone)) ?>
</p>

</body>
</html>