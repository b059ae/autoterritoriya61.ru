<?php

return array(
	'class'=>'CLogRouter',
	'routes'=>array(
		array(
			'class'=>'CFileLogRoute',
			'levels'=>'error, warning, trace',
		),
                array(
                    'class' => 'CProfileLogRoute',
                    'levels' => 'profile', //'error, warning',//profile
                    'enabled' => true,
                ),
		
	),
);